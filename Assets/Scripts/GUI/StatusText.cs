﻿using UnityEngine;
using System.Collections;

public class StatusText : MonoBehaviour {
	GUIText GuiStatusText;
	public bool DoorstatusText;
	public bool KeyPickupText;

	
	private PlayerScript playerScript;
	private DoorScript doorScript;

	// Use this for initialization
	void Start () {
		doorScript = (DoorScript)FindObjectOfType(typeof(DoorScript));
		playerScript = (PlayerScript)FindObjectOfType(typeof(PlayerScript));
		GuiStatusText = guiText;
		DoorstatusText = false;
		KeyPickupText = false;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (DoorstatusText == true){
			OpenDoor();
		} else if (DoorstatusText == false) {
			GuiStatusText.enabled = false;
		}
		
		if (KeyPickupText == true) {
			PickUpkey();
		}
	}
	
	void OpenDoor() {
		if (DoorstatusText == true){
			if (playerScript.key == false){
				GuiStatusText.enabled = true;
				GuiStatusText.color = Color.red;
				GuiStatusText.text = "You require a key to open this door!";
			} else if (doorScript.doorActive == true) {
			} else {
				GuiStatusText.enabled = true;
				GuiStatusText.color = Color.green;
				GuiStatusText.text = "Press E to open door!";
			}
		} 
	}
	
	void PickUpkey() {
		GuiStatusText.enabled = true;
		GuiStatusText.color = Color.green;
		GuiStatusText.text = "Picked up key!";
		StartCoroutine(DisableText());
	}
	
	IEnumerator DisableText() {
    	yield return new WaitForSeconds(3);
   		GuiStatusText.enabled = false;
		KeyPickupText = false;
    }
}