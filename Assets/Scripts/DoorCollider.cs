﻿using UnityEngine;
using System.Collections;

public class DoorCollider : MonoBehaviour {
	
	private DoorScript doorScript;
	private StatusText statusText;
	private PlayerScript playerScript;
	
	// Use this for initialization
	void Start () {
		//playerScript = gameObject.GetComponent<PlayerScript>();
		doorScript = (DoorScript)FindObjectOfType(typeof(DoorScript));
		statusText = (StatusText)FindObjectOfType(typeof(StatusText));
		playerScript = (PlayerScript)FindObjectOfType(typeof(PlayerScript));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == ("MainCharacter"))
		{
        	statusText.DoorstatusText = true;
		}
    }
	
	void OnTriggerStay(Collider other) {
		if (other.gameObject.tag == ("MainCharacter"))
		{
			if (Input.GetKeyDown(KeyCode.E) && playerScript.key == true){
				doorScript.doorStatus = true;
			}
		}
	}
	
	void OnTriggerExit(Collider other) {
		statusText.DoorstatusText = false;
	}
}
