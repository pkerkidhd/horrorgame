﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {
	
	GameObject door;
	public bool doorStatus;
	public bool doorActive;
	
	//private Cinematic1 cinematic1;
	
	// Use this for initialization
	void Start () {
		//cinematic1 = (Cinematic1)FindObjectOfType(typeof(Cinematic1));
		door = gameObject;
		doorStatus = false;
		doorActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (doorStatus == true){
			if (door.transform.rotation.y > 0.60) {
				doorStatus = false;
				doorActive = true;
				//cinematic1.cinematicStatus = true;
			} else {
				transform.Rotate(0,30 * Time.deltaTime, 0);
			}
		} else {
		
		}
	}
}
