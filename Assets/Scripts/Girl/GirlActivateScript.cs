﻿using UnityEngine;
using System.Collections;

public class GirlActivateScript : MonoBehaviour {
	
	private GirlScript girlScript;
	
	// Use this for initialization
	void Start () {
		girlScript = (GirlScript)FindObjectOfType(typeof(GirlScript));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == ("MainCharacter"))
		{
			girlScript.girlActive = true;
		}
    }
}
