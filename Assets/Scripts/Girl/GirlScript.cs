﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GirlScript : MonoBehaviour {
	
	public bool girlActive = false;
	
	//Waypoints
	public Transform[] waypoints;
	float duration = 1.0f;
	private Vector3 startPoint;
	private Vector3 endPoint;
	private float startTime;
	private int targetwaypoint;
	//
	private GameObject findWayPoints; 
	
	
	// Use this for initialization
	void Start () {
		
		startPoint = transform.position;
		startTime = Time.time;
		
		findWayPoints = GameObject.FindGameObjectsWithTag("Waypoint");
		
		foreach (Transform tf in waypoints) {
			Debug.Log(waypoints);
		}
		
		if(waypoints.Length <= 0) {
			Debug.Log ("No waypoints found!");
			enabled = false;
		}
		
		targetwaypoint = 0;
		endPoint = waypoints[targetwaypoint].position;
	}
	
	// Update is called once per frame
	void Update () {
		float i = (Time.time - startTime) / duration;
		transform.position = Vector3.Lerp(startPoint, endPoint, i);
		
		if (i >= 1) {
			startTime = Time.time;
			targetwaypoint++;
			targetwaypoint %= waypoints.Length;
			startPoint = endPoint;
			endPoint = waypoints[targetwaypoint].position;
		}
	}
}
