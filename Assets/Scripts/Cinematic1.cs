﻿using UnityEngine;
using System.Collections;

public class Cinematic1 : MonoBehaviour {

	Camera cinematicCam;
	public bool cinematicStatus;
	
	// Use this for initialization
	void Start () {
		cinematicCam = camera;
		cinematicStatus = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == ("MainCharacter"))
		{
			cinematicCam.camera.enabled = true;
        	cinematicCam.animation.Play("Cinematic1");
		}
    }
	
	void DisableCinematic () {
		cinematicCam.animation.Stop();
		cinematicCam.camera.enabled = false;
		DestroyObject(this.gameObject);
	}
}
