﻿using UnityEngine;
using System.Collections;

public class KeyScript : MonoBehaviour {
	private PlayerScript playerScript;
	private StatusText statusText;
	
	// Use this for initialization
	void Start () {
		playerScript = (PlayerScript)FindObjectOfType(typeof(PlayerScript));
		statusText = (StatusText)FindObjectOfType(typeof(StatusText));
		Screen.showCursor = false; 
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0.0f,100.0f * Time.deltaTime,0.0f);
	}
	
	void OnTriggerEnter(Collider other) {
    	if (other.gameObject.tag == ("MainCharacter"))
		{
			playerScript.key = true;
      	 	Destroy(gameObject);
			statusText.KeyPickupText = true;
       		Debug.Log("Picked Up key!"); 
		}
    }
}
