﻿using UnityEngine;
using System.Collections;

public class PlayerLightScript : MonoBehaviour {

	public float playerLightInt;
	Light playerLight;

	
	// Use this for initialization
	void Start () {
		playerLight = light;
	}
	
	// Update is called once per frame
	void Update () {
		if (playerLight.enabled) {
			playerLight.intensity -= 0.005f * Time.deltaTime;
			//Debug.Log(playerLight.intensity);
			if (playerLight.intensity <= 0.0f) {
				playerLight.enabled = false;
				//Debug.Log(playerLight.enabled);
			}
		}
		
		if (Input.GetKeyDown(KeyCode.F)) {
			playerLight.enabled = !playerLight.enabled;
		}
		//StartCoroutine(DisableLight());
		
	}
	
    IEnumerator DisableLight() {
    	yield return new WaitForSeconds(5);
   		
    }
}
